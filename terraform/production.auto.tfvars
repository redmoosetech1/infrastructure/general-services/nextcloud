datacenter = "Datacenter"
datastore_1 = "esx1_local_storage"
datastore_2 = "esx1_local_storage"
pool = "Resources"
network = "General"
template = "rocky8.5_server_amd64_template"
folder = "Infrastructure/Production/General"
dns_domain = "rmt"
machines = {
 nextcloud = {
  hostname = "nextcloud"
  domain = "rmt"
  ipv4_address = "172.16.30.15"
  ipv4_netmask = 24
  ipv4_gateway = "172.16.30.254"
  dns_servers = ["172.16.0.17", "172.16.0.18"]
  dns_domain = "rmt"
  num_cpus = 8
  num_cores_per_socket = 4
  memory = 8192
  disks = [
    {
      label = "disk1"
      size = 4096
      unit_number = 1
    }
  ]
 }
}
