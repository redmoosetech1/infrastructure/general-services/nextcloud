# Nextcloud

## Attention!
 **Make sure to change the owner of the Nextcloud files to www-data**
 ```
Use these commands:

chown -R www-data:www-data /srv/nextcloud
chown -R www-data:www-data /nextcloud
 ```
